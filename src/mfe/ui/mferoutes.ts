import { RouteConfig } from "@canvas/vue-router";
import AddressUpdate from "./views/AddressUpdate.vue";

/**
 * Note to @dev
 * If you plan to use <router-link> for navigation to MFE's children routes use <router-link :to={ path:supportmfeMFE.mfevm._mfeMountPath + '/childpath'} />
 * For routing to MFE's children routes, use $router.push({ path:supportmfeMFE.mfevm._mfeMountPath + '/childpath', query: { 'hello': 'world' }})
 */

/**
 * This can be used to provide redirect option to other paths
 *
 * For redirect option -
 * redirect: (to: any) => {
 *  return { path: (supportmfeMFE as any).mfevm._mfeMountPath + '/child', query: { 'hello': 'world' } }
 *    or
 *  return { name: 'child.route.name' }
 * }
 *
 * Or you can directly use
 * redirect : {'path | name':<string>} | string
 */

export const mferoutes: RouteConfig[] = [
  {
    path: "addressupdate",
    component: AddressUpdate,
    meta: {
      displayName: "AddressUpdate",
    },
  },
];
