import Vue from "vue";
import { MFE, makeChannel, connect, fetchAndLoadStyles } from "@canvas/mfe";
import Home from "./views/Home.vue";
import { supportmfeModule } from "./store";
// import VueRouter from "@canvas/vue-router";
import { mferoutes } from "./mferoutes";
/* eslint-disable */
/**
 * Fallback css list in case it is not provided from the config
 */
const mfeStyleSheets = [
  "https://naarad.livspace.com/canvas/componentlibrary/0.6.2-feature-cssupdate-dev4/components.canvas.css",
  "https://naarad.livspace.com/supportmfe/0.1.0/supportmfe.css",
];

/**
 * This is the path where webpack is configured to output the css in dev build
 * For prod builds, css shall still be loaded through the config mechanism
 */
const localStyleSheets = ["/app.css", "/chunk-vendors.css"];

function fetchMFECss(mountpoint: any): any {
  return new Promise((resolve, reject) => {
    supportmfeMFE.emitterlist.getMfeCss
      .send()
      .then((data: any) => {
        if (data) {
          // return fetchAndLoadStyles(data, mountpoint);
          Promise.all(fetchAndLoadStyles(data, mountpoint)).then(() => {
            resolve(null);
          });
        } else {
          Promise.all(fetchAndLoadStyles(mfeStyleSheets, mountpoint)).then(
            () => {
              resolve(null);
            }
          );
        }
      })
      .catch((err: any) => {
        /**
         * fallback in case when the emitterlist to get mfe css is not handled in the composer then
         * dev can set the css list in `mfeStyleSheets` and pass the same in the below function
         */
        Promise.all(fetchAndLoadStyles(mfeStyleSheets, mountpoint)).then(() => {
          resolve(null);
        });
        // return css;
      });
  });
}

const supportmfeMFE = {
  name: "supportmfe",
  sinklist: {},
  emitterlist: {
    get: makeChannel<(data: any) => Promise<any>>(),
    redirect: makeChannel<(url: string) => Promise<void>>(),
    post: makeChannel<any>(),
    getMfeCss: makeChannel<any>(),
    getConfig: makeChannel<any>(),
  },
  mfevm: null,
  store: null,
  networkMFE: null,
  routes: mferoutes,
  boot(domNode: null | HTMLElement, conf: any): Promise<any> {
    if (this.mfevm) {
      (this.mfevm as any).$destroy();
    }
    this.store = conf && conf.store;
    if (!conf.store.hasModule("supportmfe")) {
      conf.store.registerModule("supportmfe", supportmfeModule);
    }
    return new Promise((resolve, reject) => {
      if (domNode) {
        // Vue.mixin({
        //   inject: ["shadowhost"],
        //   created() {
        //     (this as any).mfedepth = conf.depth;
        //     const self = this as any;
        //     /**
        //      * This style injection can be removed now as css is loaded through external css by webpack
        //      */
        //     if (self.$style && self.$style.__inject__ && self.shadowhost) {
        //       self.$style.__inject__(self.shadowhost);
        //     }
        //   }
        // });
        // If the mfe is getting mounted by a vue router
        if (conf.router && conf.store) {
          const startupconf = {
            name: "supportmfeMFE",
            provide: {
              sinklist: this.sinklist,
              emitterlist: this.emitterlist,
              shadowhost: conf.mountpoint,
            },
            router: conf.router,
            store: conf.store,
            render: (h: any) => h(Home),
          };
          fetchMFECss(conf.mountpoint).then(() => {
            (this as any).mfevm = new Vue(startupconf);
            /**
             * Clear the stores state when the mfe is destroyed
             */
            (this as any).mfevm.$once("hook:destroyed", () => {
              conf.store.unregisterModule("supportmfe");
            });
            if (conf.depth) {
              // (this.mfevm as any).__proto__.__mfedepth = conf.depth;
              /**
               * Adding these to Vue prototype to avoid having provide/inject on the global Vue
               */
              Vue.prototype.shadowhost = conf.mountpoint;
              // Vue.prototype.__mfedepth = conf.depth;__mfedepth
            }
            (this.mfevm as any)._isMfe = true;
            (this.mfevm as any)._mfedepth = conf.depth;
            let path = conf.mfemountpath as string;
            (Object.entries as any)(conf.router.currentRoute.params).forEach(
              (entry: any[]) => {
                path = path.replace(":" + entry[0], entry[1]);
              }
            );
            (this.mfevm as any)._mfeMountPath = path;
            (this.mfevm as any).$mount(domNode);
            /**
             * Externalizing the css from webpack and loading them here in the shadowDom
             * This is being done to support both scoped and module css.
             */
            // if (process.env.NODE_ENV === "development") {
            //   fetchAndLoadStyles(localStyleSheets, conf.mountpoint);
            // }
            /**
             * mfeRef: Ref to the same MFE
             * isRedirectRequired: Whether vue router should redirect the mfe after boot is complete
             * mfemountpath is provided as conf on boot to mfe, mfe ccan choose to redirect as well
             */
            resolve({ mfeRef: supportmfeMFE, isRedirectRequired: true }); // Resolve after all sinks are subscribed to
          });
        } else {
          // this needs to be deprecated
          // If the mfe is being manually mounted
          // const startupconf = {
          //   name: "supportmfeMFE",
          //   provide: {
          //     sinklist: this.sinklist,
          //     emitterlist: this.emitterlist,
          //     shadowhost: conf.mountpoint
          //   },
          //   router: new VueRouter({
          //     mode: "history",
          //     base: "/",
          //     routes: mferoutes
          //   }),
          //   store: store,
          //   render: (h: any) => h(Home)
          // };
          // (this as any).mfevm = new Vue(startupconf);
          // (this.mfevm as any).__proto__.__mfedepth = 1;
          // (this.mfevm as any).$mount(domNode);
          // resolve(); // Resolve after all sinks are subscribed to
        }
      } else {
        reject(new Error("No mount point found!"));
      }
    });
  },
  preboot(networkMFE: MFE): void {
    if (networkMFE) {
      connect(supportmfeMFE, networkMFE);
      this.networkMFE = networkMFE as any;
    }
  },
};
/* eslint-enable */
export default supportmfeMFE;
