import mfe from "@/mfe/ui";

export interface CustomEnumsReq {
  endpoint: "launchpad.v1.custom-enums";
}

export type CustomEnumsRes = Array<{
  id: number;
  display_name: string;
  weight: number;
  name: string;
  type: string;
  is_enabled: true;
}>;

export function get(req: CustomEnumsReq): Promise<CustomEnumsRes> {
  return mfe.emitterlist.get.send({
    url: `/launchpad/v1/custom-enums?filters=type:ADDRESS_UPDATE_REASON`,
  });
}
