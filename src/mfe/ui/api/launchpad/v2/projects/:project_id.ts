import mfe from "@/mfe/ui";

export interface GetProjectIdReq {
  endpoint: "launchpad.v2.projects.:project_id";
  project_id: string | number;
}

export interface GetProjectIdRes {
  id: string | number;
  property_name: string;
  customer_display_name: string;
  address1: string | null;
  city: {
    display_name: string | null;
    name: string | null;
  };
  attributes: {
    floor_plan_available: string | null;
    preferred_language: string | null;
    budget_min: string | null | number;
    budget_max: string | null | number;
    expected_possession_timeline: string;
    property_category: string;
    addresses: string[];
  };
}

export function get(req: GetProjectIdReq): Promise<GetProjectIdRes> {
  return mfe.emitterlist.get.send({
    url: `/launchpad/v2/projects/${req.project_id}?include=city,addresses`,
  });
}
