import mfe from "@/mfe/ui";

export interface UpdateSiteAddressReq {
  endpoint: "launchpad.v2.projects.:project_id.update-site-address";
  project_id: string | number;
  data: any;
}

export interface UpdateSiteAddressRes {
  [x: string]: any;
}

export function post(req: UpdateSiteAddressReq): Promise<UpdateSiteAddressRes> {
  return mfe.emitterlist.post.send({
    url: `/launchpad/v2/projects/${req.project_id}/update-site-address`,
    body: JSON.stringify(req.data),
  });
}
