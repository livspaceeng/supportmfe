import mfe from "@/mfe/ui";

export interface GetAddressesReq {
  endpoint: "livspace-web-backend.v1.places.autocomplete";
  query: string;
  country_code: string;
}

export interface GetAddressesRes {
  [x: string]: any;
}

export function get(req: GetAddressesReq): Promise<GetAddressesRes> {
  return mfe.emitterlist.get.send({
    url: `/livspace-web-backend/v1/places/autocomplete?${req.query}`,
    headers: {
      "iso-code": req.country_code,
    },
  });
}
