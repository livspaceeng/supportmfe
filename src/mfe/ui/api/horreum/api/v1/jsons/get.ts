import mfe from "@/mfe/ui";

export interface GetConfigJSonReq {
  endpoint: "horreum.api.v1.jsons.get";
  query: string;
}

export interface GetConfigJSonRes {
  error: string;
  response: { [key: string]: any };
}

export function get(req: GetConfigJSonReq): Promise<GetConfigJSonRes> {
  return mfe.emitterlist.get.send({
    url: `/horreum/api/v1/jsons?system=${req.query}`,
  });
}
