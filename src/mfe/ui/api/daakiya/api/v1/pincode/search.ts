import mfe from "@/mfe/ui";

export interface GetPincodeSearchReq {
  endpoint: "daakiya.api.v1.pincode.search";
  data: Record<string, any>;
}

export interface GetPincodeSearchRes {
  [x: string]: any;
}

export function post(req: GetPincodeSearchReq): Promise<GetPincodeSearchRes> {
  return mfe.emitterlist.post.send({
    url: `/daakiya/api/v1/pincode/search`,
    body: JSON.stringify(req.data),
  });
}
