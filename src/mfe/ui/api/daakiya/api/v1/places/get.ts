import mfe from "@/mfe/ui";

export interface GetDaakiyaPlacesReq {
  endpoint: "daakiya.api.v1.places";
  query: string;
}

export interface GetDaakiyaPlacesRes {
  [x: string]: any;
}

export function get(req: GetDaakiyaPlacesReq): Promise<GetDaakiyaPlacesRes> {
  return mfe.emitterlist.get.send({
    url: `/daakiya/api/v1/places/${req.query}`,
  });
}
