import mfe from "@/mfe/ui";

export interface GetDaakiyaAddressReq {
  endpoint: "daakiya.v2.address.address_id";
  address_id: string;
}

export interface GetDaakiyaAddressRes {
  [x: string]: any;
}

export function get(req: GetDaakiyaAddressReq): Promise<GetDaakiyaAddressRes> {
  return mfe.emitterlist.get.send({
    url: `/daakiya/v2/address/${req.address_id}`,
  });
}
