import {
  GetBouncerUserReq,
  GetBouncerUserRes,
} from "./bouncer/v2/users/search";
import {
  GetPincodeSearchReq,
  GetPincodeSearchRes,
} from "./daakiya/api/v1/pincode/search";
import {
  GetDaakiyaPlacesReq,
  GetDaakiyaPlacesRes,
} from "./daakiya/api/v1/places/get";
import {
  GetDaakiyaAddressReq,
  GetDaakiyaAddressRes,
} from "./daakiya/v2/address/address_id";
import { GetConfigJSonReq, GetConfigJSonRes } from "./horreum/api/v1/jsons/get";
import { CustomEnumsReq, CustomEnumsRes } from "./launchpad/v1/custom-enums";
import {
  GetProjectIdReq,
  GetProjectIdRes,
} from "./launchpad/v2/projects/:project_id";
import {
  UpdateSiteAddressReq,
  UpdateSiteAddressRes,
} from "./launchpad/v2/projects/updateSiteAddress";
import {
  GetAddressesReq,
  GetAddressesRes,
} from "./livspace-web-backend/v1/places/autocomplete";

export type Response<Req> = Req extends GetConfigJSonReq
  ? GetConfigJSonRes
  : Req extends GetAddressesReq
  ? GetAddressesRes
  : Req extends GetDaakiyaPlacesReq
  ? GetDaakiyaPlacesRes
  : Req extends GetPincodeSearchReq
  ? GetPincodeSearchRes
  : Req extends GetProjectIdReq
  ? GetProjectIdRes
  : Req extends GetDaakiyaAddressReq
  ? GetDaakiyaAddressRes
  : Req extends GetBouncerUserReq
  ? GetBouncerUserRes
  : Req extends CustomEnumsReq
  ? CustomEnumsRes
  : Req extends UpdateSiteAddressReq
  ? UpdateSiteAddressRes
  : never;

export type GetRequest =
  | GetConfigJSonReq
  | GetAddressesReq
  | GetDaakiyaPlacesReq
  | GetProjectIdReq
  | GetDaakiyaAddressReq
  | GetBouncerUserReq
  | CustomEnumsReq;

export type PostRequest =
  | GetPincodeSearchReq
  | UpdateSiteAddressReq
  | { endpoint: "dummy1" }
  | { endpoint: "dummy2" };

export type Request = GetRequest | PostRequest;
