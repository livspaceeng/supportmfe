/* eslint-disable */
import { GetRequest, PostRequest, Response } from "./payloads";
import { get as getConfigJson } from "./horreum/api/v1/jsons/get";
import { get as getAddresses } from "./livspace-web-backend/v1/places/autocomplete";
import { get as getPlaceDetails } from "./daakiya/api/v1/places/get";
import { post as getPincodeSearch } from "./daakiya/api/v1/pincode/search";
import { get as getProjectDetails } from "./launchpad/v2/projects/:project_id";
import { get as getAddressById } from "./daakiya/v2/address/address_id";
import { get as getBouncerUser } from "./bouncer/v2/users/search";
import { get as getCustomEnums } from "./launchpad/v1/custom-enums";
import { post as updateSiteAddress } from "./launchpad/v2/projects/updateSiteAddress";

function assertNever(x: never): never {
  throw new Error("Check not exhaustive");
}

export function get<R extends GetRequest>(req: R): Promise<Response<R>> {
  const request = req as GetRequest;
  let response: Promise<Response<R>>;

  switch (request.endpoint) {
    case "horreum.api.v1.jsons.get":
      {
        const res = getConfigJson(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "livspace-web-backend.v1.places.autocomplete":
      {
        const res = getAddresses(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "daakiya.api.v1.places":
      {
        const res = getPlaceDetails(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "launchpad.v2.projects.:project_id":
      {
        const res = getProjectDetails(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "daakiya.v2.address.address_id":
      {
        const res = getAddressById(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "bouncer.v2.users.search":
      {
        const res = getBouncerUser(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "launchpad.v1.custom-enums":
      {
        const res = getCustomEnums(request);
        response = res as Promise<Response<R>>;
      }
      break;
    default:
      response = assertNever(request);
  }
  return response;
}

export function post<R extends PostRequest>(req: R): Promise<Response<R>> {
  const request = req as PostRequest;
  let response: Promise<Response<R>>;
  switch (request.endpoint) {
    case "daakiya.api.v1.pincode.search":
      {
        const res = getPincodeSearch(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "launchpad.v2.projects.:project_id.update-site-address":
      {
        const res = updateSiteAddress(request);
        response = res as Promise<Response<R>>;
      }
      break;
    case "dummy1":
      {
        const res = null as any;
        response = res as Promise<Response<R>>;
      }
      break;
    case "dummy2":
      {
        const res = null as any;
        response = res as Promise<Response<R>>;
      }
      break;
    default:
      response = assertNever(request);
  }
  return response;
}

export { GetRequest, PostRequest, Response } from "./payloads";
