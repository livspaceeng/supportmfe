import mfe from "@/mfe/ui";

export interface GetBouncerUserReq {
  endpoint: "bouncer.v2.users.search";
  query: string;
}

export interface GetBouncerUserRes {
  [x: string]: any;
}

export function get(req: GetBouncerUserReq): Promise<GetBouncerUserRes> {
  return mfe.emitterlist.get.send({
    url: `/bouncer/v2/users/search?${req.query}`,
  });
}
