/* eslint-disable */
import { get, post } from "../api";
import { GetAddressesRes } from "../api/livspace-web-backend/v1/places/autocomplete";

function transformAddressForDropdown(payload: GetAddressesRes) {
  return payload.map((item: Record<string, any>) => {
    item.name = item.placeId;
    item.display_name = item.description;
    return item;
  });
}

/* eslint-disable */
export const supportmfeModule = {
  namespaced: true,
  state: () => ({
    addresses: [],
    toastsList: [],
    addressChangeReasonEnums: [],
    projectCountry: "IN",
    modalElementIsVisible: false,
    composers: [],
    selectedComposer: {
      name: "",
      display_name: "",
      id: "",
      mfes: [],
      composer: {},
    },
  }),
  getters: {
    translations(state: any) {
      return state.translations;
    },
  },
  mutations: {
    setComposerConfs(state: any, payload: any) {
      state.composers = payload;
    },
    setPorjectCountry(state: any, payload: any) {
      state.projectCountry = payload;
    },
    setSelectedComposer(state: any, payload: any) {
      state.selectedComposer = Object.assign({}, payload);
      state.selectedMfe = {};
    },
    setSelectedMfe(state: any, payload: any) {
      state.selectedMfe = Object.assign({}, payload);
    },
    setSearchedAddresses(state: any, payload: any) {
      state.addresses = payload;
    },
    setModalElementVisibility(state: any, payload: any) {
      state.modalElementIsVisible = payload;
    },
    addToastItem(state: any, payload: { [key: string]: any }) {
      (state.toastsList as any).unshift(payload);
    },
    clearToast(state: any, payload: any) {
      const toastIdx = state.toastsList.findIndex(
        (toast: { [key: string]: any }) => {
          return toast.key === payload.key;
        }
      );
      if (toastIdx > -1) {
        state.toastsList.splice(toastIdx, 1);
      }
    },
  },
  actions: {
    addToast(context: any, payload: { [key: string]: any }) {
      if (!Object.keys(payload).includes("duration")) {
        payload.duration = 3000;
      }
      context.commit(
        "addToastItem",
        Object.assign(payload, { key: new Date().getTime() })
      );
      //don't clear the toasts that have 0 as duration
      if (payload.duration > 0) {
        setTimeout(() => {
          context.commit("clearToast", payload);
        }, payload.duration);
      }
    },
    async getAddressDetails(context: any, payload: string) {
      const addresses = await get({
        endpoint: "livspace-web-backend.v1.places.autocomplete",
        query: payload,
        country_code: context.state.projectCountry,
      });
      context.commit("setSearchedAddresses", addresses);
      if (addresses) {
        return transformAddressForDropdown(addresses);
      } else {
        return Promise.reject("Failed to get address");
      }
    },
    async getBouncerUserDetails(context: any, payload: string) {
      const userDetails = await get({
        endpoint: "bouncer.v2.users.search",
        query: payload,
      });
      // context.commit("setSearchedAddresses", addresses);
      if (userDetails) {
        userDetails.items = userDetails.items.map(
          (item: Record<string, any>) => {
            item.name = item.id;
            item.meta = {
              subtext: item.email,
            };
            return item;
          }
        );
        return userDetails.items;
      } else {
        return Promise.reject("Failed to get address");
      }
    },
    async getPlaceDetails(context: any, payload: string) {
      const placeDetails = await get({
        endpoint: "daakiya.api.v1.places",
        query: payload,
      });
      return placeDetails;
    },
    async getPincodeDetails(context: any, payload: string) {
      const pincodeDetails = await post({
        endpoint: "daakiya.api.v1.pincode.search",
        data: { pincodes: [payload], serviceable: true },
      });
      return pincodeDetails;
    },
    async getAddressById(context: any, payload: string) {
      const addressDetails = await get({
        endpoint: "daakiya.v2.address.address_id",
        address_id: payload,
      });
      return addressDetails;
    },
    async getProjectDetails(context: any, payload: string) {
      const projectDetails = await get({
        endpoint: "launchpad.v2.projects.:project_id",
        project_id: payload,
      });
      return projectDetails;
    },
    async updateProjectData(context: any, payload: any) {
      const updateSiteAddress = await post({
        endpoint: "launchpad.v2.projects.:project_id.update-site-address",
        project_id: payload.project_id,
        data: payload.data,
      });
      return updateSiteAddress;
    },
    async getCustomEnums(context: any, payload: string) {
      const projectEnums = await get({
        endpoint: "launchpad.v1.custom-enums",
      });
      context.state.addressChangeReasonEnums = projectEnums;
    },
  },
};
/* eslint-enable */
