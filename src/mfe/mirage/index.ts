import { MFE, makeChannel, UI } from "@canvas/mfe";
// import startServer from "./server";
import { HttpPlugin, http } from "@canvas/api";
import Vue from "vue";
declare function startServer(): void;
const mirageLayer: MFE = {
  name: "mirageLayer",
  sinklist: {
    authorize: makeChannel<() => Promise<void>>(),
    get: makeChannel<any>(),
    post: makeChannel<any>(),
    loginstatus: makeChannel<() => Promise<boolean>>(),
  },
  sinks: new Map(),
  emitterlist: {},
  emitters: new Map(),

  boot(domNode: null | HTMLElement) {
    HttpPlugin.initialize({
      auth: {
        getRefreshedAccessToken() {
          return Promise.resolve("dummyToken");
        },
        AccessToken: "dummyToken",
        BaseUrl: "http://axle.dev.livspace.com",
        CustomHeaders: {},
      },
    });
    startServer();

    this.sinklist.loginstatus.subscribe({
      type: "function",
      handler() {
        return Promise.resolve(true);
      },
    });

    this.sinklist.authorize.subscribe({
      type: "function",
      handler(data: any) {
        return Promise.resolve(true);
      },
    });

    this.sinklist.get.subscribe({
      type: "function",
      handler(data: any) {
        return http.GET(data.url, data.headers);
      },
    });

    this.sinklist.post.subscribe({
      type: "function",
      handler(data: any) {
        return http.POST(data.url, data.body, data.headers);
      },
    });

    return new Promise((resolve, reject) => {
      if (domNode) {
        new Vue({
          provide: {
            sinklist: this.sinklist,
            emitterlist: this.emitterlist,
            finishboot: resolve,
            name: this.name,
          },
          render: (h) => h(UI),
        }).$mount(domNode);
      } else {
        resolve();
      }
    });
  },
};

export default mirageLayer;
