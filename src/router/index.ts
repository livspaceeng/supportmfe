import Vue from "vue";
import VueRouter from "@canvas/vue-router";
import UILayer from "../mfe/ui";

Vue.use(VueRouter);

// @dev Dont use this router.This is just the mock composer's router
// Use mfe/ui/mferoutes to defined child routes for your mfe
// Dont install any plugin here. Instead do that in mfe/ui/index.ts

const router = new VueRouter({
  base: "/",
  mode: "history",
  routes: [
    {
      path: "/supportmfe",
      mfe: UILayer as any,
    },
  ],
});

export default router;
