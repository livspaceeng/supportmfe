import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// @dev Dont use this store.This is just the mock composer's store
// Use mfe/ui/store/index.ts to define store for your route

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
});
