import Vue from "vue";
(window as any).Vue = Vue; // TODO: Load this externally
import router from "./router";
import TestLoader from "./TestLoader.vue";
import { connect } from "@canvas/mfe";
import { supportmfeMFE, MockedNetworkLayerMFE } from "./mfe";
import AuthLayerMFE from "@canvas/authlayermfe";
import config from "../config.json";
import Vuex from "vuex";
(window as any).Vuex = Vuex;
Vue.config.productionTip = false;

/**
 * connect(supportmfeMFE, MockedNetworkLayerMFE);
 * boot the mockerlayer if needed, do the following before that
 * `chmod a+x /server/build`
 * `cd server/ && ./build`
 * then boot the mocker layer
 * MockedNetworkLayerMFE.boot(null);
 */

Vue.use(Vuex);

connect(supportmfeMFE, AuthLayerMFE);
if (config) {
  const setupPromise = AuthLayerMFE.boot(null, config).then(() => {
    return AuthLayerMFE.sinklist.authorize
      .send({})
      .then(() => {
        return AuthLayerMFE.sinklist.loginstatus.send({});
      })
      .then((isLoggedIn: boolean) => {
        if (!isLoggedIn) {
          return Promise.reject("Not logged in!");
        } else {
          return AuthLayerMFE.sinklist.loggedInUser.send({});
        }
      })
      .catch((err: any) => {
        return Promise.reject("Not logged in! Error-" + err);
      });
  });

  setupPromise.then((UserDetails: { [key: string]: any }) => {
    new Vue({
      router,
      render: (h) => h(TestLoader),
      store: new Vuex.Store({}),
    }).$mount("#app");
  });
}
