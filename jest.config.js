module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  verbose: true,
  moduleFileExtensions: ["ts", "vue", "js", "json"],
  transform: {
    "^.+\\.ts$": "ts-jest",
    ".*\\.(vue)$": "vue-jest",
    ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$":
      "jest-transform-stub",
  },
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "\\.(css|scss|sass)$": "<rootDir>/tests/__mocks__/styleMock.ts",
  },
  collectCoverage: true,
  coverageDirectory: "<rootDir>/tests/__coverage__",
  collectCoverageFrom: [
    "src/mfe/ui/components/*.{js,ts,vue}",
    "src/mfe/ui/views/*.{js,ts,vue}",
    "!**/node_modules/**",
    "!**/__coverage__/**",
    "!**/*.d.ts",
  ],
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 0,
      statements: -0,
    },
  },
};
