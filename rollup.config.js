import typescript from "rollup-plugin-typescript2";
import VuePlugin from "rollup-plugin-vue";
import commonjs from "@rollup/plugin-commonjs";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";
// import cssnano from "cssnano";
import replace from "@rollup/plugin-replace";

import eslint from "@rbnlffl/rollup-plugin-eslint";
import del from "rollup-plugin-delete";
// import css from "rollup-plugin-css-only";
import postcss from "rollup-plugin-postcss";

import { visualizer } from "rollup-plugin-visualizer";
import analyze from "rollup-plugin-analyzer";
import gzipPlugin from "rollup-plugin-gzip";
import { brotliCompressSync } from "zlib";

import url from "@rollup/plugin-url";
import path from "path";

const globals = {
  vue: "Vue",
  jquery: "$",
  "@canvas/vue-router": "@canvas/vue-router",
  vuex: "Vuex",
};
const externals = ["@canvas/vue-router", "vue", "vuex"];
export default [
  // ESM build to be used with webpack/rollup.
  {
    input: "./src/mfe/index.ts",
    shimMissingExports: false,
    output: [
      {
        file: "dist/supportmfe.umd.js",
        format: `umd`,
        name: "supportmfe",
        globals: globals,
      },
      {
        file: "dist/supportmfe.umd.min.js",
        format: `umd`,
        name: "supportmfe",
        globals: globals,
        minifyInternalExports: true,
        compact: true,
        plugins: [terser()],
      },
    ],
    external: externals,
    plugins: [
      del({ targets: "dist/*" }),
      nodeResolve({
        preferBuiltins: false,
        browser: true,
        jsnext: true,
        main: true,
      }),
      commonjs({
        include: "node_modules/**",
      }),
      typescript({
        tsconfig: "tsconfig.json",
        useTsconfigDeclarationDir: true,
      }),
      postcss({
        extensions: [".css", ".scss", ".sass"],
        extract: "supportmfe.css",
        minimize: true,
      }),
      VuePlugin({
        css: false,
        defaultLang: {
          style: "scss",
        },
        template: { transformAssetUrls: true },
        style: {
          postcssModulesOptions: {
            generateScopedName: "[local]-[hash:base64:4]",
          },
          preprocessOptions: {
            scss: {
              include: ["node_modules"],
            },
          },
        },
      }),
      url({
        emitFiles: true,
        limit: 0,
        fileName: "img/[name]_[hash][extname]",
        sourceDir: path.join(__dirname, "src"),
        destDir: path.join(__dirname, "dist"),
      }),
      replace({
        "process.env.NODE_ENV": JSON.stringify("production"),
        preventAssignment: true,
      }),
      eslint({
        fix: false,
        throwOnError: true,
        filterExclude: ["**/*.scss", "node_modules/**", "src/utils.ts"],
      }),
      // GZIP compression as .gz files
      gzipPlugin(),
      // Brotil compression as .br files
      gzipPlugin({
        customCompression: (content) =>
          brotliCompressSync(Buffer.from(content)),
        fileName: ".br",
      }),
      analyze({
        summaryOnly: true,
        hideDeps: true,
      }),
      //set open to true to evaluate the bundle visualizer
      visualizer({
        open: false,
        gzipSize: true,
        brotliSize: true,
      }),
    ],
  },
];
