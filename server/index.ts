/* eslint-disable */
import { Server } from "miragejs";
import composerconf from "./composerconf.json";

window.startServer = function() {
  new Server({
    routes() {
      this.get(
        "http://axle.dev.livspace.com/horreum/api/v1/jsons",
        (schema, req) => {
          return composerconf;
        }
      );

      this.passthrough("https://naarad.livspace.com/**");
    },
  });
};
