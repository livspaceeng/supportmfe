const path = require("path");

module.exports = {
  mode: "development",
  entry: "./index.ts",
  output: {
    filename: "mirage.js",
    path: path.resolve(__dirname, "../public"),
    libraryTarget: "umd",
  },
  devServer: {
    contentBase: path.resolve(__dirname, "public"),
    compress: true,
    port: 9090,
  },
  resolve: {
    alias: {
      vue$: "vue/dist/vue.esm.js", // 'vue/dist/vue.common.js' for webpack 1
      pdfkit: "pdfkit/js/pdfkit.standalone.js",
    },
  },
};
