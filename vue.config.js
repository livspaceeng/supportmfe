module.exports = {
  css: {
    extract: false,
    requireModuleExtension: false,
  },
  configureWebpack: (config) => {
    // for (const rule of config.module.rules) {
    //   if (rule.use && rule.use) {
    //     for (const loader of rule.use) {
    //       if (loader.options && loader.options.shadowMode === false) {
    //         loader.options.shadowMode = true;
    //         loader.options.manualInject = true;
    //       }
    //     }
    //   } else if (rule.oneOf) {
    //     for (const conf of rule.oneOf) {
    //       for (const useconf of conf.use) {
    //         if (useconf instanceof Array) {
    //           for (const loader of useconf) {
    //             if (loader.options && loader.options.shadowMode === false) {
    //               loader.options.shadowMode = true;
    //               loader.options.manualInject = true;
    //             }
    //           }
    //         } else {
    //           if (useconf.options && useconf.options.shadowMode === false) {
    //             useconf.options.shadowMode = true;
    //             useconf.options.manualInject = true;
    //           }
    //         }
    //       }
    //     }
    //   }
    // }

    // for (let i = 0; i < 4; i++) {
    //   let sassLoaderRule = config.module.rules.find((rule) =>
    //     rule.test.toString().includes("scss")
    //   ).oneOf[i].use;
    //   sassLoaderRule[0].options.sourceMap = true;
    //   sassLoaderRule[1].options.sourceMap = true;
    //   sassLoaderRule[2].options.sourceMap = true;
    //   sassLoaderRule.splice(2, 0, {
    //     loader: "resolve-url-loader",
    //     options: { sourceMap: true },
    //   });
    // }
  },
  devServer: {
    // https: true,
    disableHostCheck: true,
  },
};
